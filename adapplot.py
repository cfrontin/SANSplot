#!/usr/local/bin/python

from __future__ import division, print_function, absolute_import;

import math;
import matplotlib.pyplot as plt;
import matplotlib.ticker as tick;
import numpy as np;
import pandas as pd;
import seaborn as sns;
import scipy.optimize as opt;
import sys;
from os.path import expanduser;

from SANSplot.utils import *;

# default start up BS
home_dir= expanduser("~");

# prep for tufte ready plots
sns.set(style= 'ticks', palette= 'Set2');

# prep for latex-like plots
plt.rc('text', usetex= True);
plt.rc('font', family= 'serif');
plt.rc('axes.spines', top= False,
                      bottom= False,
                      left= False,
                      right= False);

def loadAdapData(filedir_adapt, case_spec= None, colnames_in= None,
                 verbose= False):
  ## loads adaptation history data from a directory and packs it for use
  #
  # this function takes a directory where an adaptive solve is depositing its
  # outfiles, runs through the various adaptation cases therein, and returns
  # the adaptation data in a family of dataframes to plot them

  # default column names for an test.adapthist file
  if colnames_in is None:
    colnames_adapthist= ("iter", "DOFtrue", "DOFest", "DOFpred", "errorInd",
                        "errorEst", "errorTrue", "output", "evalCount",
                        "objRed", "elem");
  else:
    # if user specifies colnames, make sure defaults are included
    assert("iter" in colnames_in);
    assert("DOFtrue" in colnames_in);
    assert("DOFest" in colnames_in);
    assert("DOFpred" in colnames_in);
    assert("errorInd" in colnames_in);
    assert("errorEst" in colnames_in);
    assert("errorTrue" in colnames_in);
    assert("output" in colnames_in);
    assert("evalCount" in colnames_in);
    assert("objRed" in colnames_in);
    colnames_adapthist= colnames_in;

  # get the sub directories that match the format dof*p*
  cases= get_subdirectories(filedir_adapt);

  # filter the cases to the specification
  filterCasesToSpecification(cases, case_spec);

  # debug
  if verbose:
    counter= 0;
    print("\nloaded cases:");
    for case in cases:
      print("\tcase %d:" % counter, end= '\t');
      print("p= %d" % case['p'], end= '\t');
      print("dof= %d" % case['dofReq'], end= '\t');
      print("filename: %s" % case['filename']);
      counter += 1;
    print();

  if verbose:
    print("loading files into dataframes!");

  # now we have the list of cases and their filenames: loop over cases
  for case in cases:
    try:
      data_adapthist= pd.read_fwf(case['filename'],
                                  names= colnames_adapthist, skiprows= 1);
      assert (len(colnames_adapthist) == data_adapthist.shape[1]), \
          "number of column names and number of columns need to match!";
      case['data']= data_adapthist;

      if verbose:
        print("\t%s" % case['filename'], end= ' ');
        print("loaded. data shape:", end= ' ');
        print(case['data'].shape);
    except pd.errors.EmptyDataError:
      case['data']= None;
      if verbose:
        print("\tno data found!");

  if verbose:
    print();

  return cases;

def adapPlot(filedir, case_spec= None, colnames= None,
             spinesOn= False, output_dir= None, verbose= False):
  ## a function to make an adaptation plot
  #
  # this function takes a file directory input and makes an adaptation plot for
  # the cases found in that directory.

  # get the metadata
  md= parseFilenameMetadata(filedir, verbose= verbose);
  # get the text fillers
  mdtxt= convertMetadata2Text(md);

  # spines on if you can't handle full Tufte
  if spinesOn:
    plt.rc('axes.spines', top= False,
                          bottom= True,
                          left= False,
                          right= True);

  # load the data
  cases= loadAdapData(filedir, colnames_in= colnames, case_spec= case_spec,
                      verbose= verbose);

  # figure out the unique p that occur here
  p_unique= [];
  for case in cases:
    p_here= case['p'];
    if p_here not in p_unique:
      p_unique.append(p_here);

  fig_col= [];
  axes_col= [];

  for p in p_unique:

    # create a new dataframe
    (fig, axes)= plt.subplots(4, 1, sharex= True, sharey= False,
                              figsize= (8, 6), dpi= 80);

    for case in cases:
      if case['data'] is not None:
        if (case['p'] == p):
          data= case['data'];
          iter= data['iter'];
          DOFtrue= data['DOFtrue'];
          errorInd= data['errorInd'];
          errorEst= data['errorEst'];
          errorTrue= data['errorTrue'];

          lnplt= axes[0].plot(iter, DOFtrue);
          axes[0].plot(iter, case['dofReq']*np.ones(iter.shape), '--', \
              color= lnplt[0].get_color(), alpha= 0.3);
          axes[0].set_yscale('log');
          axes[0].yaxis.set_tick_params(which= 'minor', length= 2.5);
          axes[0].yaxis.set_tick_params(which= 'major', length= 4.0);

          axes[1].plot(iter, errorInd);
          axes[1].set_yscale('log');
          axes[1].yaxis.set_tick_params(which= 'minor', length= 2.5);
          axes[1].yaxis.set_tick_params(which= 'major', length= 4.0);

          axes[2].plot(iter, np.abs(errorEst));
          axes[2].set_yscale('log');
          axes[2].yaxis.set_tick_params(which= 'minor', length= 2.5);
          axes[2].yaxis.set_tick_params(which= 'major', length= 4.0);

          axes[3].plot(iter, np.abs(errorTrue));
          axes[3].set_yscale('log');
          axes[3].yaxis.set_tick_params(which= 'minor', length= 2.5);
          axes[3].yaxis.set_tick_params(which= 'major', length= 4.0);

        # make the common x-axis look good
        fig.gca().xaxis.set_major_locator(tick.MaxNLocator(nbins= 5,
            integer= True, prune= 'upper'));

        # labeling
        plt.xlabel("iterations");
        axes[0].set_ylabel("$N_\\mathrm{DOF}$", rotation= 0, labelpad= 30,
                           verticalalignment= 'center');
        axes[0].yaxis.tick_right();
        axes[1].set_ylabel("indicated\nerror", rotation= 0, labelpad= 30,
                           verticalalignment= 'center');
        axes[1].yaxis.tick_right();
        axes[2].set_ylabel("estimated\nerror", rotation= 0, labelpad= 30,
                           verticalalignment= 'center');
        axes[2].yaxis.tick_right();
        axes[3].set_ylabel("true\nerror", rotation= 0, labelpad= 30,
                           verticalalignment= 'center');
        axes[3].yaxis.tick_right();

        title_text= "Adaptation history for " + mdtxt['dimension'] + " " \
            + mdtxt['physics_short'] + " with $p= %d$ " % p \
            + mdtxt['discretization'];
        plt.suptitle(title_text, y= 0.96, fontsize= 14);

        # tighten up around shuffled labels and ticks
        plt.tight_layout();
        plt.subplots_adjust(top= 0.9); # tight_layout neglects suptitle

    # output
    if output_dir is None:
      plt.show();
    else:
      filename_out= output_dir + "/adapthist_" \
          + mdtxt['dimension'].replace('+', '') + "_" \
          + mdtxt['discretization'] + "_p" + str(p) + ".eps";
      plt.savefig(filename_out);

    fig_col.append(fig);
    axes_col.append(axes);

  return (fig_col, axes_col);
