#!/usr/local/bin/python

from __future__ import division, print_function, absolute_import;

import math;
import matplotlib.pyplot as plt;
import matplotlib.ticker as tick;
import numpy as np;
import pandas as pd;
import seaborn as sns;
import scipy.optimize as opt;
import sys;
from os.path import expanduser;

from SANSplot.utils import *;
from SANSplot.adapplot import loadAdapData;

# default start up BS
home_dir= expanduser("~");

# prep for tufte ready plots
sns.set(style= 'ticks', palette= 'Set2');

# prep for latex-like plots
plt.rc('text', usetex= True);
plt.rc('font', family= 'serif');
plt.rc('axes.spines', top= False,
                      bottom= False,
                      left= False,
                      right= False);

def loadOutputData(filedir, plotTrueError= True, case_spec= None,
                   colnames_in= None, verbose= False):
  ## loads output convergence data from directory, and packs it up for use

  # borrow loading from adaptation
  cases= loadAdapData(filedir, case_spec, colnames_in, verbose);

  # make sure the cases have true error if necessary
  if plotTrueError:
    for case in cases:
      if case['data'] is not None:
        assert ("errorTrue" in case['data'].columns.values);
        foundTrueError= False;
        if not np.all(case['data'].errorTrue.values == 0.0):
          foundTrueError= True;
        assert foundTrueError;

  # return the case data
  return cases;

def loadConvData(filedir_convergence, meta= None, lastn= 5, doOutput= False,
                 colnames_in= None, verbose= False):
  ## loads convergence history data from a file, and packs it for use
  #
  # this function takes a file, loads the convhist.test-style file at that
  # address, then 1) load and 2) prep the data in it for a convergence plot
  # with the idea being that the file input will likely be an adaptation with
  # multiple realizations at a given x-axis grouping location

  # filename is "test.convhist", add it to the end of the file directory
  filename_default= "test.convhist";
  filename_convergence= filedir_convergence + "/" + filename_default;

  # default column names for a convhist.test file
  if colnames_in is None:
    colnames_convhist= ("p", "DOFreq", "hDOF", "Nelem", "Ndof", "L2error");
  else:
    # if user specifies colnames, make sure defaults are included
    assert("p" in colnames_in);
    assert("DOFreq" in colnames_in);
    assert("hDOF" in colnames_in);
    assert("Nelem" in colnames_in);
    assert("Ndof" in colnames_in);
    assert("L2error" in colnames_in);
    colnames_convhist= colnames_in;

  # load the data
  data_convhist= pd.read_csv(filename_convergence, names= colnames_convhist,
                             skiprows= 1);

  # load output data
  data_adapthist= loadOutputData(filedir_convergence, plotTrueError= True,
                                verbose= verbose);

  if doOutput:
    data_convhist['output']= np.nan;
    data_convhist['trueOutputError']= np.nan;

    for case in data_adapthist:
      print("p= %d dofReq= %d" % (case['p'], case['dofReq']));

      p= case['p'];
      dofReq= case['dofReq'];

      convMatched= data_convhist.loc[(data_convhist.p == p) & (data_convhist.DOFreq == dofReq)];
      if (len(convMatched) == 0) or (convMatched.shape[0] == 0):
        if verbose:
          print("no matching outputs found for p= %d, dofReq= %d." % (p, dofReq));
      else:
        countMax= np.min((convMatched.shape[0], lastn));
        convMatched= convMatched.iloc[-countMax:, :]; # reselect to match sizes
        print("convMatched:");
        print(convMatched);

        adapMatched= case['data'].iloc[-countMax:, :]; # reselect to match sizes
        print("adapMatched:");
        print(adapMatched);

        convMatched.loc[:, 'output']= adapMatched.output.values;
        convMatched.loc[:, 'trueOutputError']= adapMatched.errorTrue.values;
        print("convMatched update:");
        print(convMatched);

        print(convMatched.index.values);
        print(adapMatched.output.values);
        data_convhist.loc[convMatched.index.values, 'output']= \
            adapMatched.output.values;
        data_convhist.loc[convMatched.index.values, 'trueOutputError']= \
            adapMatched.errorTrue.values;

    # last but not least make sure that we sanitize cases w/o output data
    data_convhist.dropna(subset= ["output", "trueOutputError"], inplace= True);

  if meta is not None:
    # augment data with hElem
    dim= 1.0*meta['is1D'] + 2.0*meta['is2D'] + 3.0*meta['is3D'] + 4.0*meta['is4D'];
    prefix= np.power(2., 0.5*dim)/np.sqrt(dim + 1.);
    V_total= 1.; # naive assumption may want to change
    data_convhist['hElem']= np.power(prefix*V_total/data_convhist['Nelem'], 1./dim);

  # mean data
  datamean_convhist= data_convhist.copy().groupby(by= ['p', 'DOFreq']) \
      .mean().reset_index();

  if verbose:
    print("file loaded into dataframe!");
    print("\t%s\n" % filename_convergence);

  # return a tuple of the data and the mean data
  return (data_convhist, datamean_convhist);

def calculateConvergenceStats(df_in, lastn, xVar= 'hDOF', yVar= 'L2error',
                              p_check= None, DOFreq_check= None):
  ## calculates the convergence stats for a series
  #
  # calculates the convergence rate via a linear fit of the log of xVar vs the
  # log of yVar, using for the fit the lastn values, and in the process
  # optionally makes sure that the unique requests from the mean data used here
  # matches the unique requests from the raw data, which can be handed in

  # unique requests
  p_unique= df_in.p.unique().astype(int);
  DOFreq_unique= df_in.DOFreq.unique().astype(int);

  # make sure mean unique and raw unique requests match
  print(p_unique);
  print(p_check);
  if p_check is not None:
    assert(set(p_unique) == set(p_check));
  if DOFreq_check is not None:
    assert(set(DOFreq_unique) == set(DOFreq_check));

  # get ready to calculate slopes
  fullslopes= {};
  endslopes= {};
  xest= {};
  yest= {};

  # for each unique order, calculate mean convergence rate
  for p in p_unique:
    xfit= np.log(np.abs(df_in.loc[df_in.p == p, xVar].values));
    yfit= np.log(np.abs(df_in.loc[df_in.p == p, yVar].values));
    # print("xfit:", end= ' ');
    # print(xfit);
    # print("yfit:", end= ' ');
    # print(yfit);
    isort= np.argsort(xfit);
    mbfull= np.polyfit(xfit, yfit, 1);
    fullslopes[p]= mbfull[0]; # slope from polyfit
    xfit= xfit[isort];
    yfit= yfit[isort];
    # print("xfit*:", end= ' ');
    # print(xfit);
    # print("yfit*:", end= ' ');
    # print(yfit);
    xfit= xfit[0:lastn];
    yfit= yfit[0:lastn];
    mb= np.polyfit(xfit, yfit, 1);
    endslopes[p]= mb[0]; # slope from truncated polyfit
    xest[p]= np.exp(xfit); # slope calculation points
    yest[p]= np.exp(mb[0]*xfit + mb[1]); # linfit values
    # print("xfit**:", end= ' ');
    # print(xfit);
    # print("yfit**:", end= ' ');
    # print(mb[0]*xfit + mb[1]);

  return (endslopes, fullslopes, xest, yest);

def convPlot(filename, convlast= 2,
             xVar= "hDOF", yVar= "L2error",
             pSelect= None,
             spinesOn= False, fig= None, ax= None, verbose= False):
  ## a function to make a convergence plot
  #
  # this function takes a dataframe input and makes a convergence plot
  # @param    filename  the file that is handed in
  # @param    fig       a figure to continue to write in (if exists)
  # @param    ax        an axes to continue to write in (if exists)

  # get the metadata
  md= parseFilenameMetadata(filename, verbose= verbose);
  # get the text fillers
  mdtxt= convertMetadata2Text(md);

  # # release from service if request is not for adaptive case (no true output)
  # if (not md['isAdaptive']) and (yVar == "trueOutputError"):
  #   return (None, None, md);

  # spines on if you can't handle full Tufte
  if spinesOn:
    plt.rc('axes.spines', top= False,
                          bottom= True,
                          left= True,
                          right= False);

  # load the data
  doOutput= ((yVar == "trueOutputError") or (yVar == "output"));
  (data_convhist, datamean_convhist)= loadConvData(filename, meta= md,
                                                   verbose= verbose,
                                                   doOutput= doOutput);

  # grab unique polynomial orders and convergence rate
  p_unique= data_convhist.p.unique().astype(int);
  DOFreq_unique= data_convhist.DOFreq.unique().astype(int);

  if verbose:
    print("p_unique:", end= ' ');
    print(p_unique);
    print("DOFreq_unique:", end= ' ');
    print(DOFreq_unique);
    print();

  # calculate convergence data
  (convRateLast, convRateFull, xest, yest)= \
      calculateConvergenceStats(datamean_convhist,
                                convlast,
                                xVar= xVar, yVar= yVar,
                                p_check= p_unique,
                                DOFreq_check= DOFreq_unique);

  if verbose:
    print("found: convRateLast:", end= ' ');
    print(convRateLast);
    print("       convRatefull:", end= ' ');
    print(convRateFull);
    print();

  # generate all text to go onto plot
  text_title= mdtxt['discretization'] + " " + mdtxt['refinement'] \
                  + " refinement of " + mdtxt['dimension'] + " " \
                  + mdtxt['physics'];

  if xVar == "hDOF":
    text_xlabel= "mesh characteristic length, $\overline{h}_\mathrm{DOF}= " + \
                   "1/\sqrt[" + str(mdtxt['numND']) + "]{DOF}$";
  elif xVar == "Ndof":
    text_xlabel= "number of elements, $N_\mathrm{elem}$";
  elif xVar == "hElem":
    text_xlabel= "mesh characteristic length, $\overline{h}_\mathrm{elem} " + \
                     "\sim 1/\sqrt[" + str(mdtxt['numND']) + \
                     "]{N_\mathrm{elem}}$";
  else:
    raise LookupError("not ready to write that xlabel.");

  if yVar == "L2error":
    text_ylabel= "error,\n$||u - u_\mathrm{exact}||_{L^2}$";
  elif yVar == "trueOutputError":
    text_ylabel= "true output error,\n$||\mathcal{J} - \mathcal{J}_\mathrm{exact}||$";
  elif yVar == "output":
    text_ylabel= "output,\n$\mathcal{J}$";
  else:
    raise LookupError("not ready to write that ylabel.");

  if verbose:
    print("ready to print plot data:");
    print("\ttitle:\t" + '"' + text_title + '"');
    print("\txlabel:\t" + '"' + text_xlabel + '"');
    print("\tylabel:\t" + '"' + text_ylabel.replace('\n', '\\n') + '"');
    print();

  # generate xticks and xticks labels
  xticks= DOFreq_unique[::3].astype(float)**(-1./float(mdtxt['numND']));
  xticklabels= [];
  for DOFreq in DOFreq_unique[::3]:
      xticklabels.append(("$\sqrt[-%d]{%d}$" % (mdtxt['numND'], DOFreq)));
  if verbose:
    print("hugh-style fancy xticks still broken.");
    print("\txticks:", end= ' ');
    print(xticks);
    print("\txticklabels:", end= ' ');
    print(xticklabels);
    print();

  # details for aesthetics
  alpha_grid= 0.7;
  alpha_grid_minor= 0.15;
  legend_loc= 'best';
  dot_size= 12.5;
  line_size= 2.5;

  # create plot and axes
  if ((fig is None) and (ax is None)):
      fig= plt.figure(figsize= (8, 6), dpi= 80);
      ax= fig.gca();

  # loop over unique orders
  for p_current in np.sort(p_unique):

      if pSelect is not None and p_current in pSelect:
        continue;

      text_refinelabel= "";
      if md['isAdaptive']:
          text_refinelabel= "\\textsuperscript{a}";
      elif md['isStructured']:
           text_refinelabel= "\\textsuperscript{s}";
      else:
           text_refinelabel= "\\textsuperscript{u}";

      xData= data_convhist[data_convhist.p == p_current][xVar];
      yData= data_convhist[data_convhist.p == p_current][yVar];
      xMeanData= datamean_convhist[datamean_convhist.p == p_current][xVar];
      yMeanData= datamean_convhist[datamean_convhist.p == p_current][yVar];

      if (yVar is "trueOutputError"):
        yData= np.abs(yData);
        yMeanData= np.abs(yMeanData);

      lineType= "-";
      desat= None;
      plot_color= None;

      desat= 1.0;

      if md['isAdaptive']:
        lineType= "-";
      elif not md['isStructured']:
        lineType= "--";
      else:
        lineType= "-.";

      # if p_current == 0:
      #     colorset= "colorblind";
      # elif p_current == 1:
      #   colorset= "bright";
      # elif p_current == 2:
      #   colorset= "pastel";
      # elif p_current == 3:
      #   colorset= "dark";
      # else:
      #   raise LookupError("not ready to choose a color for p= 4+.");

      # if md['isDG']:
      #   plot_color= sns.color_palette(colorset, desat= desat)[0];
      # elif md['isCG']:
      #   plot_color= sns.color_palette(colorset, desat= desat)[1];
      # elif md['isVMSD']:
      #   plot_color= sns.color_palette(colorset, desat= desat)[2];

      plot_color= sns.color_palette("pastel")[convPlot.colorcounter];
      convPlot.colorcounter += 1;

      text_label= mdtxt['discretization'] + text_refinelabel \
          + ", $p= " + str(p_current) + "$, slope: %.3f" % convRateLast[p_current];
      plt.scatter(xData, yData, c= plot_color, s= dot_size, label= None);
      lnplt= plt.plot(xMeanData, yMeanData, lineType,
                      c= plot_color, lw= line_size, label= text_label);
      plt.plot(xest[p_current], 0.75*yest[p_current], ':',
               color= lnplt[0].get_color(), alpha= 0.5, linewidth= 2.5);
  #     plt.xticks([]);
  #     plt0= plt.plot([], [], '-.');
  #     leg_items.append(plt0);
  #     leg_text.append("$p= " + str(p_current) + "$");
  ax.set_xscale('log');
  if yVar is not "output":
    ax.set_yscale('log');
  # ax.xaxis.set_ticks(xticks);
  # ax.xaxis.set_tick_labels(xticklabels);

  plt.title(text_title, fontsize= 14);
  plt.xlabel(text_xlabel);
  plt.ylabel(text_ylabel, rotation= 0, labelpad= 45);
  # print leg_items
  # print leg_text
  # ax.legend(leg_items, leg_text);
  ax.legend(loc= legend_loc, frameon= True,
            facecolor= 'white', edgecolor= 'white', framealpha= 1.0);
  # ax.legend(loc= legend_loc, fancybox= True);
  plt.grid(which= 'major', alpha= alpha_grid);
  plt.grid(which= 'minor', alpha= alpha_grid_minor);

  # tighten up around shuffled labels and ticks
  plt.tight_layout();

  return (fig, ax, md);
convPlot.colorcounter= 0;

