#!/usr/local/bin/python

from __future__ import division, print_function, absolute_import;

import glob;

def get_subdirectories(filedir_adapt):
  ## return the subdirectory dictionaries from an adaptive problem directory
  #
  # ???

  cases= [];

  # adaptive-style files
  dirs= glob.glob(filedir_adapt + "/dof*p*");
  for dir in dirs:
    dir= dir.split("/")[-1];
    vals= dir.split("p");
    p= int(vals[1]);
    dof= int(vals[0].split("dof")[1]);
    filename_case= filedir_adapt + dir + "/test.adapthist";
    case= {'p': p, 'dofReq': dof, 'filename': filename_case};
    cases.append(case);

  return cases;

def filterCasesToSpecification(cases, case_spec= None, verbose= False):
  ## take all the cases that were found, and filter them against a specification
  #
  # ???

  # if specifying a case or a set of cases, make sure each appears in the cases
  if case_spec is not None:
    speclist= []; # for (dofReq, p) tuples

    # two types of input allowed ... tuple of two or list of multiple
    if type(case_spec) is tuple:
      # then the user specified one case
      assert len(case_spec) == 2, "tuple input must be length 2";
      dofReq= case_spec[0];
      p= case_spec[1];
      speclist.append((dofReq, p));
    elif type(case_spec) is list:
      # user specified multiple cases
      for specification in case_spec:
        assert len(specification) == 2, "tuples in list must be length 2";
        dofReq= specification[0];
        p= specification[1];
        speclist.append((dofReq, p));
    else:
      raise Exception("user specifications not recognized.");

    # loop over the specifications input to make sure we have a directory for it
    for specification in speclist:
      # that specification
      p_spec= specification[1];
      dofReq_spec= specification[0];

      found= False;

      # loop over cases in directory
      for case in cases:
        p= case['p'];
        dofReq= case['dofReq'];
        if (p == p_spec) and (dofReq == dofReq_spec):
          found= True;

      assert found, "specified case was not found!";

    # loop over cases and clean out any that aren't specified
    to_remove= []; # can't natively remove while iterating, so storage bad ones
    for case in cases:
      p= case['p'];
      dofReq= case['dofReq'];

      found= False;

      # loop over specifications
      for specification in speclist:
        # that specification
        p_spec= specification[1];
        dofReq_spec= specification[0];
        if (p == p_spec) and (dofReq == dofReq_spec):
          found= True;

      # if this case wasn't specified, remove it!
      if not found:
        # print("removing case with p= %d, dofReq= %d" % (p, dofReq));
        to_remove.append(case);
      # else:
      #   print("not removing case with p= %d, dofReq= %d" % (p, dofReq));

    # now go back and delete
    for case in to_remove:
      cases.remove(case);

    if verbose:
      print("\nuser specifications noted:");
      for specification in speclist:
        print("\tp=", end= " ");
        print(specification[1], end= "\t");
        print("dofReq=", end= " ");
        print(specification[0]);


def convertMetadata2Text(metadata):
  ## go from metadata to a textual descriptions
  #
  # this takes a dict of metadata and creates a twin dict of text helpers for
  # the metadata, for printing to stdout or to label plots and stuff.
  # @param      metadata      a dict of metadata booleans
  # @return                   a dict of textual insights of interest

  # discretization type
  text_galerkin= "";
  if metadata['isDG']:
    text_galerkin= "DG";
  elif metadata['isCG']:
    text_galerkin= "CG";
  elif metadata['isVMSD']:
    text_galerkin= "VMSD";
  else:
    raise NotImplementedError("not ready to handle that case.");

  # uniform v. adaptive
  text_refine= "adaptive";
  if not metadata['isAdaptive']:
      if metadata['isStructured']:
          text_refine= "structured uniform";
      else:
          text_refine= "unstructured uniform";

  # dimensionality
  dimless= 1 if metadata['isSpacetime'] else 0;
  numND= metadata['is1D']*1 + metadata['is2D']*2 + metadata['is3D']*3 + metadata['is4D']*4;
  text_dimension= str(numND - dimless) + "D" \
      + ("+T" if metadata['isSpacetime'] else "");

  # physics
  text_physics= "";
  text_physics_short= "";
  if metadata['isAD']:
    text_physics= "advection-diffusion";
    text_physics_short= "AD";
  elif metadata['isNS']:
      text_physics= "Navier-Stokes";
      text_physics_short= "NS";
  elif metadata['isPM']:
      text_physics= "porous media flow";
      text_physics_short= "PM";
  elif metadata['isL2']:
      text_physics= "$L^2$ projection";
      text_physics_short= "$L^2$";
  elif metadata['isLZ']:
      text_physics= "Lorenz system";
      text_physics_short= "LZ";
  else:
      raise NotImplementedException;

  textual= {};
  textual['discretization']= text_galerkin;
  textual['refinement']= text_refine;
  textual['dimension']= text_dimension;
  textual['physics']= text_physics;
  textual['physics_short']= text_physics_short;
  textual['numND']= numND;

  return textual;

def parseFilenameMetadata(filename, verbose= False):
  ## parse a filename for information about the plot
  #
  # this takes a filename and figures out what we think it should be a plot of
  # and also validates to make sure that combination is sensible
  #
  # @param      filename        the filename we want to evaluate
  # @param      verbose         verbosity setting, default False
  # @return                     a dict with the metadata we grepped

  # a dict to store case metadata
  metadata= {};

  # rip the case name itself out
  case_name= filename.split("/")[-2];

  if verbose:
    print("\nparsing %s." % filename);
    print("figured out case name: %s" % case_name);

  # rip out details from the case name
  metadata['isAdaptive']= "adap" in case_name;
  metadata['isSpacetime']= "ST" in case_name;
  metadata['isStructured']= (not metadata['isAdaptive']) and ("unstructured" not in case_name);

  metadata['is4D']= "4D" in case_name;
  metadata['is3D']= "3D" in case_name;
  metadata['is2D']= "2D" in case_name;
  metadata['is1D']= "1D" in case_name;

  metadata['isAD']= "AD" in case_name;
  metadata['isNS']= "NS" in case_name;
  metadata['isPM']= "PM" in case_name;
  metadata['isL2']= "L2" in case_name;
  metadata['isLZ']= "LZ" in case_name;

  metadata['isDG']= "DG" in case_name;
  metadata['isCG']= "CG" in case_name;
  metadata['isGLS']= "GLS" in case_name;
  metadata['isVMSD']= "VMSD" in case_name;

  if verbose:
    # print the result
    print("\nparsing results:");
    print("\tisAdaptive?:\t", str(metadata['isAdaptive']));
    print("\tisSpacetime?:\t", str(metadata['isSpacetime']));
    print("\tisStructured?:\t", str(metadata['isStructured']));
    print("\tis4D?:\t\t", str(metadata['is4D']));
    print("\tis3D?:\t\t", str(metadata['is3D']));
    print("\tis2D?:\t\t", str(metadata['is2D']));
    print("\tisAD?:\t\t", str(metadata['isAD']));
    print("\tisNS?:\t\t", str(metadata['isNS']));
    print("\tisPM?:\t\t", str(metadata['isPM']));
    print("\tisL2?:\t\t", str(metadata['isL2']));
    print("\tisDG?:\t\t", str(metadata['isDG']));
    print("\tisCG?:\t\t", str(metadata['isCG']));
    print("\tisGLS?:\t\t", str(metadata['isGLS']));
    print("\tisVMSD?:\t", str(metadata['isVMSD']));
    print("");

  # make sure only one of 2D, 3D, or 4D is active
  assert((metadata['is1D'] and not metadata['is2D'] and not metadata['is3D'] and not metadata['is4D']) or
         (not metadata['is1D'] and metadata['is2D'] and not metadata['is3D'] and not metadata['is4D']) or
         (not metadata['is1D'] and not metadata['is2D'] and metadata['is3D'] and not metadata['is4D']) or
         (not metadata['is1D'] and not metadata['is2D'] and not metadata['is3D'] and metadata['is4D']));

  # make sure only one physics is active
  assert((metadata['isAD'] and not metadata['isNS']
              and not metadata['isPM'] and not metadata['isL2'] and not metadata['isLZ']) or
         (not metadata['isAD'] and metadata['isNS']
              and not metadata['isPM'] and not metadata['isL2'] and not metadata['isLZ']) or
         (not metadata['isAD'] and metadata['isNS']
              and not metadata['isPM'] and not metadata['isL2'] and not metadata['isLZ']) or
         (not metadata['isAD'] and not metadata['isNS']
              and metadata['isPM'] and not metadata['isL2'] and not metadata['isLZ']) or
         (not metadata['isAD'] and not metadata['isNS']
              and not metadata['isPM'] and metadata['isL2'] and not metadata['isLZ']) or
         (not metadata['isAD'] and not metadata['isNS']
              and not metadata['isPM'] and not metadata['isL2'] and metadata['isLZ']));

  # make sure only one discretization is active
  assert((metadata['isDG'] and not metadata['isCG']
              and not metadata['isGLS'] and not metadata['isVMSD']) or
         (not metadata['isDG'] and metadata['isCG']
              and not metadata['isGLS'] and not metadata['isVMSD']) or
         (not metadata['isDG'] and not metadata['isCG']
              and metadata['isGLS'] and not metadata['isVMSD']) or
         (not metadata['isDG'] and not metadata['isCG']
              and not metadata['isGLS'] and metadata['isVMSD']));

  # return the metadata struct
  return metadata;
